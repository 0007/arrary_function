<?php
echo "Array_combine: <br>";

$a = array('green', 'red', 'yellow');
$b = array('avocado', 'apple', 'banana');
$c = array_combine($a, $b);

echo "<pre>";
print_r($c);

echo "<br>";
print_r(array_combine(Array('a','b','c'), Array(1,2,3)));

echo "<br>";
print_r(array_combine(Array('a','a','b'), Array(1,2,3)));

echo "<br>";
print_r(array_combine(Array('a','a','a'), Array(1,2,3)));
?>

